$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/filtrarProductos.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: your.email@your.domain.com"
    },
    {
      "line": 2,
      "value": "#Keywords Summary :"
    },
    {
      "line": 3,
      "value": "#Feature: List of scenarios."
    },
    {
      "line": 4,
      "value": "#Scenario: Business rule through list of steps with arguments."
    },
    {
      "line": 5,
      "value": "#Given: Some precondition step"
    },
    {
      "line": 6,
      "value": "#When: Some key actions"
    },
    {
      "line": 7,
      "value": "#Then: To observe outcomes or validation"
    },
    {
      "line": 8,
      "value": "#And,But: To enumerate more Given,When,Then steps"
    },
    {
      "line": 9,
      "value": "#Scenario Outline: List of steps for data-driven as an Examples and \u003cplaceholder\u003e"
    },
    {
      "line": 10,
      "value": "#Examples: Container for s table"
    },
    {
      "line": 11,
      "value": "#Background: List of steps run before each of the scenarios"
    },
    {
      "line": 12,
      "value": "#\"\"\" (Doc Strings)"
    },
    {
      "line": 13,
      "value": "#| (Data Tables)"
    },
    {
      "line": 14,
      "value": "#@ (Tags/Labels):To group Scenarios"
    },
    {
      "line": 15,
      "value": "#\u003c\u003e (placeholder)"
    },
    {
      "line": 16,
      "value": "#\"\""
    },
    {
      "line": 17,
      "value": "## (Comments)"
    },
    {
      "line": 18,
      "value": "#Sample Feature Definition Template"
    }
  ],
  "line": 19,
  "name": "Filtrar Productos",
  "description": "Para poder visualizar de diferente forma los productos\r\nComo comprador\r\nQuiero un filtro de productos que a la vez pueda ordenar por nombre y precio",
  "id": "filtrar-productos",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 24,
  "name": "Ordenar productos",
  "description": "",
  "id": "filtrar-productos;ordenar-productos",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 25,
  "name": "Ingresé a Ebay",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "hice una búsqueda de \"\u003cCategoria\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "selecciono la marca: \"\u003cmarca\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "selecciono la talla \"\u003ctalla\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "imprimo el número de resultados",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "ordeno los resultados por \"\u003cOrdenar\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "verifico el orden \"\u003cOrdenar\u003e\" de los \"\u003cNumResultadosAValidar\u003e\" primeros resultados",
  "keyword": "Then "
});
formatter.examples({
  "line": 33,
  "name": "",
  "description": "",
  "id": "filtrar-productos;ordenar-productos;",
  "rows": [
    {
      "cells": [
        "Categoria",
        "marca",
        "talla",
        "numResultados",
        "Ordenar",
        "NumResultadosAValidar"
      ],
      "line": 34,
      "id": "filtrar-productos;ordenar-productos;;1"
    },
    {
      "cells": [
        "Calzado",
        "PUMA",
        "10",
        "25",
        "Precio + Envío: más bajo primero",
        "5"
      ],
      "line": 35,
      "id": "filtrar-productos;ordenar-productos;;2"
    },
    {
      "cells": [
        "Calzado",
        "PUMA",
        "10",
        "25",
        "Precio + Envío: más alto primero",
        "5"
      ],
      "line": 36,
      "id": "filtrar-productos;ordenar-productos;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 35,
  "name": "Ordenar productos",
  "description": "",
  "id": "filtrar-productos;ordenar-productos;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 25,
  "name": "Ingresé a Ebay",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "hice una búsqueda de \"Calzado\"",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "selecciono la marca: \"PUMA\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "selecciono la talla \"10\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "imprimo el número de resultados",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "ordeno los resultados por \"Precio + Envío: más bajo primero\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "verifico el orden \"Precio + Envío: más bajo primero\" de los \"5\" primeros resultados",
  "matchedColumns": [
    4,
    5
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "filtrarProductosSteps.ingresé_a_Ebay()"
});
formatter.result({
  "duration": 878560478,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Calzado",
      "offset": 22
    }
  ],
  "location": "filtrarProductosSteps.hice_una_búsqueda_de(String)"
});
formatter.result({
  "duration": 20391159906,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PUMA",
      "offset": 22
    }
  ],
  "location": "filtrarProductosSteps.selecciono_la_marca(String)"
});
formatter.result({
  "duration": 1491154509,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 21
    }
  ],
  "location": "filtrarProductosSteps.selecciono_la_talla(String)"
});
formatter.result({
  "duration": 2330546994,
  "status": "passed"
});
formatter.match({
  "location": "filtrarProductosSteps.imprimo_el_número_de_resultados()"
});
formatter.result({
  "duration": 75564111,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Precio + Envío: más bajo primero",
      "offset": 27
    }
  ],
  "location": "filtrarProductosSteps.ordeno_los_resultados_por(String)"
});
formatter.result({
  "duration": 1525652143,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Precio + Envío: más bajo primero",
      "offset": 19
    },
    {
      "val": "5",
      "offset": 61
    }
  ],
  "location": "filtrarProductosSteps.verifico_el_orden_de_los_primeros_resultados(String,String)"
});
formatter.result({
  "duration": 424218179,
  "error_message": "java.lang.AssertionError: expected:\u003c49.28\u003e but was:\u003c56.12\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:118)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat com.belatrix.page.filtrarProductosPage.verificarResultador(filtrarProductosPage.java:107)\r\n\tat com.belatrix.steps.filtrarProductosSteps.verifico_el_orden_de_los_primeros_resultados(filtrarProductosSteps.java:45)\r\n\tat ✽.Then verifico el orden \"Precio + Envío: más bajo primero\" de los \"5\" primeros resultados(src/test/resources/features/filtrarProductos.feature:31)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 36,
  "name": "Ordenar productos",
  "description": "",
  "id": "filtrar-productos;ordenar-productos;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 25,
  "name": "Ingresé a Ebay",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "hice una búsqueda de \"Calzado\"",
  "matchedColumns": [
    0
  ],
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "selecciono la marca: \"PUMA\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "selecciono la talla \"10\"",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "imprimo el número de resultados",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "ordeno los resultados por \"Precio + Envío: más alto primero\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "verifico el orden \"Precio + Envío: más alto primero\" de los \"5\" primeros resultados",
  "matchedColumns": [
    4,
    5
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "filtrarProductosSteps.ingresé_a_Ebay()"
});
formatter.result({
  "duration": 37885805,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Calzado",
      "offset": 22
    }
  ],
  "location": "filtrarProductosSteps.hice_una_búsqueda_de(String)"
});
formatter.result({
  "duration": 9361178738,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "PUMA",
      "offset": 22
    }
  ],
  "location": "filtrarProductosSteps.selecciono_la_marca(String)"
});
formatter.result({
  "duration": 1622494519,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "10",
      "offset": 21
    }
  ],
  "location": "filtrarProductosSteps.selecciono_la_talla(String)"
});
formatter.result({
  "duration": 1738476720,
  "status": "passed"
});
formatter.match({
  "location": "filtrarProductosSteps.imprimo_el_número_de_resultados()"
});
formatter.result({
  "duration": 70139611,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Precio + Envío: más alto primero",
      "offset": 27
    }
  ],
  "location": "filtrarProductosSteps.ordeno_los_resultados_por(String)"
});
formatter.result({
  "duration": 1435724684,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Precio + Envío: más alto primero",
      "offset": 19
    },
    {
      "val": "5",
      "offset": 61
    }
  ],
  "location": "filtrarProductosSteps.verifico_el_orden_de_los_primeros_resultados(String,String)"
});
formatter.result({
  "duration": 430261893,
  "error_message": "java.lang.AssertionError: expected:\u003c2557.87\u003e but was:\u003c2515.16\u003e\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.failNotEquals(Assert.java:834)\r\n\tat org.junit.Assert.assertEquals(Assert.java:118)\r\n\tat org.junit.Assert.assertEquals(Assert.java:144)\r\n\tat com.belatrix.page.filtrarProductosPage.verificarResultador(filtrarProductosPage.java:107)\r\n\tat com.belatrix.steps.filtrarProductosSteps.verifico_el_orden_de_los_primeros_resultados(filtrarProductosSteps.java:45)\r\n\tat ✽.Then verifico el orden \"Precio + Envío: más alto primero\" de los \"5\" primeros resultados(src/test/resources/features/filtrarProductos.feature:31)\r\n",
  "status": "failed"
});
});