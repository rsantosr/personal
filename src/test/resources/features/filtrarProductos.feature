#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
Feature: Filtrar Productos
  Para poder visualizar de diferente forma los productos
  Como comprador
  Quiero un filtro de productos que a la vez pueda ordenar por nombre y precio

  Scenario Outline: Ordenar productos
    Given Ingresé a Ebay
    And hice una búsqueda de "<Categoria>"
    When selecciono la marca: "<marca>"
    And selecciono la talla "<talla>"
    And imprimo el número de resultados
    And ordeno los resultados por "<Ordenar>"
    Then verifico el orden "<Ordenar>" de los "<NumResultadosAValidar>" primeros resultados

    Examples: 
      | Categoria | marca | talla | numResultados | Ordenar                          | NumResultadosAValidar |
      | Calzado   | PUMA  |    10 |            25 | Precio + Envío: más bajo primero |                     5 |
      | Calzado   | PUMA  |    10 |            25 | Precio + Envío: más alto primero |                     5 |
