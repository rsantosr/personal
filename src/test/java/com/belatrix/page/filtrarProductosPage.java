package com.belatrix.page;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.By;
import com.belatrix.driver.BrowserDriver;
import com.mibanco.ui.util.ControlUI;

public class filtrarProductosPage extends BrowserDriver {

	private By btnComprarPor = By.id("gh-shop-a");
	private By Categoria = By.linkText("?");
	private By lblVerTodo = By.xpath("//span[text()='Calzado para Hombre']//following::span[text()='Ver Todo']");
	private By chkMarca = By.xpath("//span[text()='?']");
	private By chkTalla = By.xpath("//span[text()='?']");

	private By lblResultados = By.xpath("//h1[contains(text(),'resultados')]");
	private By cmbOrdenar = By.xpath("//div[text()='Mejor resultado']");
	private By cmbOrdenado = By.xpath("//div[text()='?']");
	private By cmbOpcion = By.xpath("//span[text()='?']");

	private By lblPrecio = By
			.xpath("//li[@id='srp-river-results-listing?']//following::span[contains(text(),'S/.')][1]");

	public void verificarEbay() {
		assertEquals("Comprar por categoría", driver.findElement(btnComprarPor).getText());
	}

	public void ingresarACategoria(String strCategoria) {
		driver.findElement(btnComprarPor).click();

		Categoria = ControlUI.reemplazarValorEnIdentificador("linkText", Categoria, "?", strCategoria);
		System.out.println("Rafe: " + Categoria.toString());
		ControlUI.esperaObjeto(Categoria);
		driver.findElement(Categoria).click();
		ControlUI.scrollByVisibleElement(lblVerTodo);
		driver.findElement(lblVerTodo).click();
	}

	public void filtrarMarca(String strMarca) {
		chkMarca = ControlUI.reemplazarValorEnIdentificador("xpath", chkMarca, "?", strMarca);
		ControlUI.scrollByVisibleElement(chkMarca);
		driver.findElement(chkMarca).click();

	}

	public void filtrarTalla(String strTalla) {
		chkTalla = ControlUI.reemplazarValorEnIdentificador("xpath", chkTalla, "?", strTalla);
		ControlUI.esperaObjeto(chkTalla);
		driver.findElement(chkTalla).click();
	}

	public void imprimirNumeroResultados() {
		ControlUI.esperaObjeto(lblResultados);
		System.out.println(driver.findElement(lblResultados).getText());
	}

	public void ordenarResultados(String strOrden) {
		ControlUI.moverMOuse(cmbOrdenar);
		cmbOpcion = ControlUI.reemplazarValorEnIdentificador("xpath", cmbOpcion, "?", strOrden);
		driver.findElement(cmbOpcion).click();

	}

	public void verificarResultador(String strOrden, String strNumResultados) {
		cmbOrdenado = ControlUI.reemplazarValorEnIdentificador("xpath", cmbOrdenado, "?", strOrden);
		ControlUI.esperaObjeto(cmbOrdenado);
		String strPrecio;
		List<Double> lstListaPrecios = new ArrayList<Double>();
		List<Double> lstNoOrdenadaPrecio = new ArrayList<Double>();
		for (int i = 1; i <= Integer.parseInt(strNumResultados); i++) {
			By precioTmp = lblPrecio;
			precioTmp = ControlUI.reemplazarValorEnIdentificador("xpath", precioTmp, "?", String.valueOf(i));
			ControlUI.scrollByVisibleElement(precioTmp);
			strPrecio = driver.findElement(precioTmp).getText();
			if (strPrecio.indexOf("a") > -1) {
				strPrecio = strPrecio.substring(strPrecio.indexOf(".") + 2, strPrecio.indexOf("a") - 1).replace(" ",
						"");
			} else {
				strPrecio = strPrecio.substring(strPrecio.indexOf(".") + 2).replace(" ", "");

			}

			System.out.println("Precio de lista " + i + ": " + strPrecio);
			lstListaPrecios.add(Double.parseDouble(strPrecio));
		}
		for (Double item : lstListaPrecios) {
			lstNoOrdenadaPrecio.add(item);
		}

		switch (strOrden) {
		case "Precio + Envío: más bajo primero":
			Collections.sort(lstListaPrecios);
			break;

		case "Precio + Envío: más alto primero":
			Collections.sort(lstListaPrecios, Collections.reverseOrder());
			break;

		default:
			break;
		}

		for (int j = 0; j < lstListaPrecios.size(); j++) {
			assertEquals(lstListaPrecios.get(j), lstNoOrdenadaPrecio.get(j));
		}

	}

}
