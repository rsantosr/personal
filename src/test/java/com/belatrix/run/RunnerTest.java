package com.belatrix.run;

import java.net.MalformedURLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.belatrix.driver.BrowserDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {  
		                       "src/test/resources/features/filtrarProductos.feature"
							}, 
				glue = { "com.belatrix.steps" }, 
				tags = { "~@ignore" }, 
				plugin = { "pretty",
						   "json:target/cucumber-reports/Cucumber.json", "junit:target/cucumber-reports/Cucumber.xml",
						   "html:target/cucumber-reports/cucumber-pretty" },
				monochrome = true)
public class RunnerTest{

	@BeforeClass
	public static void iniciarAppium() throws MalformedURLException, Exception {
		WebDriver driver = BrowserDriver.getCurrentDriver("chrome");
		try {
			driver.get("https://www.ebay.com/");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@AfterClass
	public static void CerrarNavegador() {
		BrowserDriver.cerrarPantallas("chrome");
		BrowserDriver.close();
	}
	

}
