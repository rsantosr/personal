package com.belatrix.steps;

import com.belatrix.page.filtrarProductosPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class filtrarProductosSteps {

	filtrarProductosPage productPage = new filtrarProductosPage();

	@Given("^Ingresé a Ebay$")
	public void ingresé_a_Ebay() throws Throwable {
		productPage.verificarEbay();
	}

	@Given("^hice una búsqueda de \"([^\"]*)\"$")
	public void hice_una_búsqueda_de(String strCategoria) throws Throwable {
		productPage.ingresarACategoria(strCategoria);
	}

	@Given("^selecciono la marca: \"([^\"]*)\"$")
	public void selecciono_la_marca(String strMarca) throws Throwable {
		productPage.filtrarMarca(strMarca);
	}

	@When("^selecciono la talla \"([^\"]*)\"$")
	public void selecciono_la_talla(String strTalla) throws Throwable {
		productPage.filtrarTalla(strTalla);
	}

	@When("^imprimo el número de resultados$")
	public void imprimo_el_número_de_resultados() throws Throwable {
		productPage.imprimirNumeroResultados();
	}

	@When("^ordeno los resultados por \"([^\"]*)\"$")
	public void ordeno_los_resultados_por(String strOrden) throws Throwable {
		productPage.ordenarResultados(strOrden);
	}

	@Then("^verifico el orden \"([^\"]*)\" de los \"([^\"]*)\" primeros resultados$")
	public void verifico_el_orden_de_los_primeros_resultados(String strOrden, String strNumResultados) throws Throwable {
		productPage.verificarResultador(strOrden, strNumResultados);
	}

}
