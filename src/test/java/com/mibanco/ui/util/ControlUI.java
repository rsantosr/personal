package com.mibanco.ui.util;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.belatrix.driver.BrowserDriver;

public class ControlUI extends BrowserDriver {

	public void gotoURL(String url) {
		driver.get(url);
	}

	public static void scrollByVisibleElement(By element) {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement Element = driver.findElement(element);
			js.executeScript("arguments[0].scrollIntoView();", Element);
		} catch (NoSuchElementException e) {
			System.out.println("No se encontro elemento: "+e.getMessage());
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	
	
	public static void moverMOuse(By element) {
		try {
			new Actions(driver).moveToElement(driver.findElement(element)).build()
			.perform();
		}catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	
	}
	
	public static By reemplazarValorEnIdentificador(String IdAReemplazar, By element, String strValorQuitar, String strValorAgregar) {
		By byReemplazo=element;
		String auxXpath = element.toString();
		auxXpath=auxXpath.substring(auxXpath.indexOf(":")+2);
		auxXpath = auxXpath.replace(strValorQuitar, strValorAgregar);
		
		switch (IdAReemplazar.toLowerCase()) {
		case "linktext":
			byReemplazo=By.linkText(auxXpath);
			break;
			
		case "xpath":
			byReemplazo=By.xpath(auxXpath);
			break;

		default:
			break;
		}
		return byReemplazo;
	}
	public static void esperaObjeto(WebElement Objeto) {
		WebDriverWait wait = new WebDriverWait(driver, 45);
		wait.until(ExpectedConditions.elementToBeClickable(Objeto));
	}
	public static void esperaObjeto(By Objeto) {
		WebDriverWait wait = new WebDriverWait(driver, 45);
		wait.until(ExpectedConditions.elementToBeClickable(Objeto));
	}

}
